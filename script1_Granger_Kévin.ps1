#Kraizix
#19/10/2020
#Resolution of TP1 https://gitlab.com/it4lik/b1-workstation/-/tree/master/tp/1
#This script shows various information about your computer

"Get average ping latency to 10.33.3.253 server..."
""
$rep_time = (Test-Connection -ComputerName "10.33.3.253" -Count 4  | measure-Object -Property ResponseTime -Average).average

"Get download and upload speed..."
""
$DownloadURL = "https://bintray.com/ookla/download/download_file?file_path=ookla-speedtest-1.0.0-win64.zip"
$DownloadLocation = "$($Env:ProgramData)\SpeedtestCLI"
try {
    $TestDownloadLocation = Test-Path $DownloadLocation
    if (!$TestDownloadLocation) {
        new-item $DownloadLocation -ItemType Directory -force
        Invoke-WebRequest -Uri $DownloadURL -OutFile "$($DownloadLocation)\speedtest.zip"
        Expand-Archive "$($DownloadLocation)\speedtest.zip" -DestinationPath $DownloadLocation -Force
    } 
}
catch {  
    write-host "The download and extraction of SpeedtestCLI failed. Error: $($_.Exception.Message)"
    exit 1
}
$SpeedtestResults = & "$($DownloadLocation)\speedtest.exe" --format=json --accept-license --accept-gdpr
$SpeedtestResults | Out-File "$($DownloadLocation)\LastResults.txt" -Force
$SpeedtestResults = $SpeedtestResults | ConvertFrom-Json
 
[PSCustomObject]$SpeedtestObj = @{
    downloadspeed = [math]::Round($SpeedtestResults.download.bandwidth / 1000000 * 8, 2)
    uploadspeed   = [math]::Round($SpeedtestResults.upload.bandwidth / 1000000 * 8, 2)
}
Write-Host -NoNewline "Name : "
[system.environment]::MachineName
""
Write-Host -NoNewLine "Ip : "
Write-Host $(ipconfig | Where-Object {$_ -match 'IPv4.+\s(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})' } | out-null; $Matches[1])
""
Write-Host -NoNewline "OS : "
(Get-WmiObject Win32_OperatingSystem).Caption
""
Write-Host -NoNewline "Version : "
(Get-WmiObject Win32_OperatingSystem).Version
""
Get-Command -Module WindowsUpdateProvider
""
$Updates = Start-WUScan -SearchCriteria "IsInstalled=0 AND IsHidden=0 AND IsAssigned=1"
if (!$Updates)
{
    "Is OS up-to-date : True"
}
else
{
    "Is OS up-to-date : False"
}
$Uptime = $(Get-WmiObject Win32_OperatingSystem).LastBootUpTime
""
Write-Host "Uptime : $Uptime"
""
$Taille_RAM_MAX=[STRING]((Get-WmiObject -Class Win32_ComputerSystem ).TotalPhysicalMemory/1GB)
$Taille_RAM_LIBRE=[STRING]((Get-WmiObject -Class Win32_OperatingSystem).FreePhysicalMemory/1MB)
$Taille_RAM_UTILISE=[STRING]($Taille_RAM_MAX - $Taille_RAM_LIBRE)
"RAM :"
Write-Host "    Free : $Taille_RAM_LIBRE Gb"
Write-Host "    Used : $Taille_RAM_UTILISE Gb"
$Taille_Disk_Max = [STRING]((Get-WmiObject Win32_LogicalDisk).Size/1GB)
$Taille_Disk_Free = [STRING]((Get-WmiObject Win32_LogicalDisk).FreeSpace/1GB)
$Taille_Disk_Utilise=[STRING]($Taille_Disk_Max - $Taille_Disk_Free)
""
"Disk :"
Write-Host "    Free : $Taille_Disk_Free Gb"
Write-Host "    Used : $Taille_Disk_Utilise Gb"
""
$Users =(Get-LocalUser).Name
Write-Host "Users : $Users"
""
Write-Host "10.33.3.253 Average Ping : $rep_time ms"
$download =$SpeedtestObj.downloadspeed
$upload = $SpeedtestObj.uploadspeed
Write-Host "Download speed : $download Mbps"
Write-Host "Upload speed : $upload Mbps"