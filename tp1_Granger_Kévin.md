# TP 1
## I/ Self-Footprinting
### Host os
#### Windows/Pc
```
Get-WmiObject Win32_OperatingSystem | Select-Object Caption, Version, OSArchitecture, CSName

Caption                      Version    OSArchitecture CSName
-------                      -------    -------------- ------
Microsoft Windows 10 Famille 10.0.18363 64 bits        LAPTOP-DS2IE5NI
```
#### RAM

```
Get-CimInstance win32_physicalmemory | Format-Table Configuredclockspeed, Capacity, Manufacturer

Configuredclockspeed   Capacity Manufacturer
--------------------   -------- ------------
                2667 8589934592 SK Hynix
                2667 8589934592 SK Hynix
```

### Devices
#### Processeur
```
get-wmiobject Win32_processor | Format-Table NumberOfLogicalProcessors, NumberOfCores, Name

NumberOfLogicalProcessors NumberOfCores Name
------------------------- ------------- ----
                        8             4 Intel(R) Core(TM) i5-9300H CPU @ 2.40GHz
```
Le i5 correspond à la famille du processeur, le 9 de 9300 correspond à la génération du processeur (ici 9e génération), le 300 correspond au numéro du processeur il permet de différencier les caractéristiques des différents processeurs, et le H signifie que c'ets un processeur avec des hautes perfomances destinées aux ordinateurs portables
#### TouchPad/TrackPad
```Get-PnpDevice -InstanceId 'HID\ELAN1203&Col01\5&6decf3a&0&0000' | Get-PnpDeviceProperty

InstanceId KeyName                                   Type       Data
---------- -------                                   ----       ----
HID\ELA... DEVPKEY_Device_DeviceDesc                 String     Souris HID
HID\ELA... DEVPKEY_Device_HardwareIds                StringList {HID\VEN_ELAN&DEV_1203&Col01, HID\ELAN1203&Col01, HI...
HID\ELA... DEVPKEY_Device_CompatibleIds              StringList {}
HID\ELA... DEVPKEY_Device_Service                    String     mouhid
HID\ELA... DEVPKEY_Device_Class                      String     Mouse
HID\ELA... DEVPKEY_Device_ClassGuid                  Guid       {4D36E96F-E325-11CE-BFC1-08002BE10318}
HID\ELA... DEVPKEY_Device_Driver                     String     {4d36e96f-e325-11ce-bfc1-08002be10318}\0000
HID\ELA... DEVPKEY_Device_ConfigFlags                UInt32     0
HID\ELA... DEVPKEY_Device_Manufacturer               String     Microsoft
[...]
```

#### CG
```
Get-CimInstance Win32_VideoController | Format-Table Name

Name
----
Intel(R) UHD Graphics 630
NVIDIA GeForce GTX 1650
```
### Disque dur
```
Get-CimInstance Win32_DiskDrive

DeviceID           Caption                    Partitions Size         Model
--------           -------                    ---------- ----         -----
\\.\PHYSICALDRIVE0 Micron_2200V_MTFDHBA512TCK 3          512105932800 Micron_2200V_MTFDHBA512TCK
```

```
Get-Partition


   DiskPath : \\?\scsi#disk&ven_nvme&prod_micron_2200v_mtf#5&2119f054&0&000000#{53f56307-b6bf-11d0-94f2-00a0c91efb8b}

PartitionNumber  DriveLetter Offset                                                                                                   Size Type
---------------  ----------- ------                                                                                                   ---- ----
1                            1048576                                                                                                260 MB System
2                            273678336                                                                                               16 MB Reserved
3                C           290455552                                                                                           475.35 GB Basic
4                            510694260736                                                                                          1.32 GB Recovery
```

### Users
```
Get-LocalUser

Name               Enabled Description
----               ------- -----------
Administrateur     False   Compte d’utilisateur d’administration
DefaultAccount     False   Compte utilisateur géré par le système.
Invité             False   Compte d’utilisateur invité
kevin              True
WDAGUtilityAccount False   Compte d’utilisateur géré et utilisé par le système pour les scénarios Windows Defender Application Guard.
```

### Processus
```
get-process

Handles  NPM(K)    PM(K)      WS(K)     CPU(s)     Id  SI ProcessName
-------  ------    -----      -----     ------     --  -- -----------
    495      28    25804      38792       0,66  12084   8 ApplicationFrameHost
   1892      56    78160      77676     107,72   4256   0 ArmouryCrate.Service
    878      52    35796      58736     143,17  15984   8 ArmouryCrate.UserSessionHelper
    207      18     3580       1432       0,53  17612   8 ArmourySocketServer
    276      20    29696       1480       1,70  14660   8 ArmourySwAgent
    171       7     1232       5028       0,06   4576   0 AsHidCtrlService
    388      74    25544      14308      18,48  16728   8 asus_framework
    158      11     1652       1084       0,13   1352   8 ASUS_FRQ_Control
    297      13     3032      10164       0,22   4284   0 AsusLinkNear
     63       4      900       3188       0,08   4272   0 AsusLinkNearExt
    293      14     7708      13872       0,70   4248   0 AsusLinkRemote
    282      10     7416       8856      14,56   4300   0 AsusMultiAntennaSvc
    175      10     2160       6396       0,98   3724   0 AsusOptimization
    288      14     2756      14192       0,30   5136   8 AsusOptimizationStartupTask
    161      13     2340      12100       0,11  18260   8 AsusOSD
    268      14     1568       6512       0,19   4316   0 AsusPTPService
    470      19     9484      20824      17,47   4240   0 AsusSoftwareManager
    398      22    32648      34284       0,73   6600   8 AsusSoftwareManagerAgent
   1431      14     4560      15064       2,14   4264   0 AsusSystemAnalysis
    160       9     1796       7468       0,06   4308   0 AsusSystemDiagnosis
    289      19    15444      26864       0,16  18084   8 backgroundTaskHost
    604      30    38448        452       1,53  11296   8 Calculator
    323      19    60496      77120       4,14   2420   8 Code
    751      37    33484      74096      37,70   6624   8 Code
    412      24    38604      65648       3,05   7996   8 Code
    590      43   296780     125860      60,83  17284   8 Code
    263      16     9808      15312       0,05  19560   8 Code
    436      20    12392      25812       0,48  19908   8 Code
    489      33   138396     178984     119,36  20324   8 Code
    204      14     7696      10860       0,03  16020   8 CodeHelper
    159       7     1588       8444       0,05   9464   8 CompPkgSrv
    151       9     1876       8160       0,03   3568   8 conhost
    118       7     6420       5128       0,00   3936   0 conhost
    115       8     6520      11024       0,30   6284   8 conhost
    261      14     7028      18156      42,80  13576   8 conhost
    118       8     6520      11008       0,03  16648   8 conhost
    121       8     6528       1068       0,47  19344   8 conhost
    852      26     2144       5288       6,08    840   0 csrss
    386       8     1776       5300      25,33   2296   3 csrss
    809      32     4232       7980     252,17   8740   8 csrss
    360       8     1720       5664       2,59  14548   7 csrss
    373       8     1760       5092       3,11  14668   4 csrss
    427       8     1888       5620     544,58  17104   5 csrss
    437      15     4012      15264      31,48  17096   8 ctfmon
    472      25    14112      27616       7,89   3456   8 Discord
    622      36   113260     141632     133,13   3680   8 Discord
    260      18     9652      14888       0,06   6804   8 Discord
    327      21    11004      18612       0,33  11608   8 Discord
    859      46    35880      77080      85,78  12720   8 Discord
   1047     130   324936     291012   1 952,42  16096   8 Discord
    247      23     5636      14364       0,47   6180   8 dllhost
    195      16     3372       9788       0,05  12128   0 dllhost
   1198      55   140472     160568     686,53  12480   8 dwm
    112       7     1592       5456       0,05   4464   0 esif_uf
   2917     123    68052     161184     163,78  10904   8 explorer
     32       5     1568       2356       0,13   1204   0 fontdrvhost
     32       7     2380       6276       0,58  14780   8 fontdrvhost
     88       5     1168       4544       2,13   4568   0 ibtsiva
      0       0       60          8                 0   0 Idle
    174       9     1976       8548       0,16   2836   0 igfxCUIService
    472      18     5052      18364       2,08  20212   8 igfxEM
    163       9     1988       7572       0,20   3972   0 Intel_PIE_Service
    152       8     1688       7260       0,06   4324   0 IntelCpHDCPSvc
    138       7     1608       6664       0,03   5756   0 IntelCpHeciSvc
    139       8     1232       5628       0,00   5396   0 jhi_service
    625      25    16236      21752     651,69   4684   0 LightingService
    585      28    29636      55832       1,48  15792   8 LockApp
     43       7     1208       2936       0,11    104   0 LsaIso
   1659      43     9492      21808      64,75    316   0 lsass
      0       0      728      56216     134,63   2760   0 Memory Compression
    309      17     6712      20316       1,84    148   8 msedge
    275      19    31688      64632       3,67   1324   8 msedge
    344      23    58296      90104       6,41   1624   8 msedge
    554      32    36704      61224      81,47   2344   8 msedge
    295      23    63604      97084     119,56   7208   8 msedge
    320      21    38096      73016       1,70   7480   8 msedge
    348      22    52300      89480       4,22   8008   8 msedge
    296      20    42408      77492       2,77   8352   8 msedge
    377      31    87732     123228     111,11   9288   8 msedge
    426      32   122904     163588     337,41  10036   8 msedge
    347      26    74860     104688      55,44  10168   8 msedge
    260      19   141268     170664      45,94  10204   8 msedge
    231      15    11724      23696       0,08  10256   8 msedge
    303      20    29820      61144       1,95  10416   8 msedge
    312      24    60804      97176      18,30  10948   8 msedge
    316      24    57004      91328      14,22  11772   8 msedge
    304      20    42976      75448       2,13  12140   8 msedge
    385      25    82552     122236       4,00  12920   8 msedge
   2515      86   242608     338896     700,31  13500   8 msedge
    287      19    48896      86784       6,50  13676   8 msedge
    247      17    26840      52124       0,77  13892   8 msedge
    720      29   142024     175924      72,08  15544   8 msedge
    257      18    28912      69352       1,30  15776   8 msedge
    831      74   422240     248380     694,13  17452   8 msedge
    301      20    35872      68488       3,00  17728   8 msedge
    369      31   169180     200436      76,80  18032   8 msedge
    394      14     3668      12868       0,50  19736   8 msedge
   2213      88   540684     413152   1 416,61   4668   0 MsMpEng
    392      19    11004      24940      45,33   4608   0 NahimicService
    281      15     4256       1700       1,72   3068   8 NahimicSvc32
    463      16     6540       1468       1,56   8748   8 NahimicSvc64
    195      12     8232      12380      17,14  11304   0 NisSrv
    730     237    13868      32244      27,86   4616   0 nvcontainer
    372      26     9428      24208       9,45   9872   8 nvcontainer
    386      17     5600      17204       5,53   2380   0 NVDisplay.Container
    604      27    27828      35308       5,20   3096   8 NVDisplay.Container
    583      75    35720       3228       5,92   1308   8 NVIDIA Web Helper
    748      28    39632      48584       8,97   4592   0 OfficeClickToRun
    396      26    41624       2824       1,11   7744   8 P508PowerAgent
    470      27    62588      71984       1,72   8116   8 powershell
   1125      54   209876     226176      17,91   9168   8 powershell
    233      27    24292      16348       0,14   4180   0 PresentationFontCache
      0      63     9432        164       0,00   9584   0 RefreshRateService
      0      12     7056      81468       3,20    128   0 Registry
    399      18     5008      15912       3,97   4636   0 ROGLiveService
    305      12     2376       1016       0,30   2060   8 RtkAudUService64
    292      11     2304       8712       0,25   4600   0 RtkAudUService64
    205      12     2600      12844       0,25   1708   8 RuntimeBroker
    319      18     6684      26184       4,13  10668   8 RuntimeBroker
    702      32    13860      45492       5,98  12828   8 RuntimeBroker
    389      21     8604      32352       3,33  12996   8 RuntimeBroker
    292      15     3320      19996       0,05  13340   8 RuntimeBroker
    225      14     3664      20384       0,38  14132   8 RuntimeBroker
    357      19     6340      21592       3,81  14508   8 RuntimeBroker
    301      16     5520      22596       0,45  14800   8 RuntimeBroker
    738      72    36496      47540      78,38   9808   0 SearchIndexer
   1344     115   135976     213268      33,58   2672   8 SearchUI
      0       0      184      40324       0,00     72   0 Secure System
    696      17     6188      16028       6,06   9484   0 SecurityHealthService
    159      10     1988       9092       0,13  19424   8 SecurityHealthSystray
    806      11     5988      10676      93,59   1016   0 services
    428      18     5224       6988       3,13  18720   8 SettingSyncHost
    101       6     5816       6464       9,91  16148   0 SgrmBroker
    702      30    17924      63984       2,97   3488   8 ShellExperienceHost
    619      18     7364      27540       6,02  10880   8 sihost
    136       8     2336       7124       0,05  15508   8 smartscreen
     65       3     1160       1132       0,25    580   0 smss
    268      17     3804      17016       0,23   7256   8 SnippingTool
    469      37    31072       3736       0,69   7272   8 SonicStudio3
    431      21     5432      12840       0,31   3892   0 spoolsv
    649      31    43168      79832       3,95   8036   8 StartMenuExperienceHost
    484      31    53456      64496      30,03  16440   8 SteelSeriesEngine3
    179      10     2028       6592       0,02    144   0 svchost
    142      12     1728       5844       0,30   1068   0 svchost
     86       5      948       3832       0,00   1140   0 svchost
   1336      28    17520      36164      92,94   1172   0 svchost
   1411      20    11736      19092     137,59   1332   0 svchost
    406      11     3628       8932      34,91   1400   0 svchost
    197      15     2044       7676       0,36   1592   0 svchost
    196      13     2040      10784       0,42   1604   0 svchost
    260      16     2648      10652       0,45   1616   0 svchost
    104       9     1240       4968       0,03   1628   0 svchost
    255      13     2644      10404       3,50   1764   0 svchost
    157       9     2232      11128       0,95   1772   0 svchost
    446      18     7476      15980      42,50   1884   0 svchost
    172       7     2124       5904       1,94   1908   0 svchost
    225      13     6080      12376       4,80   1976   0 svchost
    257      10     2380       8436       0,53   1988   0 svchost
    450      14    17056      18252       5,89   2080   0 svchost
    204      11     2020       8312       0,09   2100   0 svchost
    349      11     3024      10040      18,27   2224   0 svchost
    162      27     7624      10924      17,73   2324   0 svchost
    223      12     2400      10288       0,14   2388   0 svchost
    230      10     2532       7408      27,89   2408   0 svchost
    125       8     1660       6584       0,08   2468   0 svchost
    132       8     1420       5924       0,06   2556   0 svchost
    257       7     1336       5500       0,73   2604   0 svchost
    226      11     2656      12596     115,77   2612   0 svchost
    173       9     1980       7256       0,44   2620   0 svchost
    393      17     5004      12580      49,64   2628   0 svchost
    182      12     2116       8064       0,95   2752   0 svchost
    608      21    11684      21104     192,91   2816   0 svchost
    286      13     4112       8796     185,39   2848   0 svchost
    206       9     1916       7352       0,56   2912   0 svchost
    179      11     2380       8560       2,06   2924   0 svchost
    881      15     3488       9344      29,86   3028   0 svchost
    472      15     4292      14260      15,25   3136   0 svchost
    281      10     8604      16960      70,59   3276   0 svchost
    377      17     2972       9496       4,98   3372   0 svchost
    139      12     1748       6128       1,50   3384   0 svchost
    252      12     2708       8908      11,20   3504   0 svchost
    563      24     7472      19892      20,09   3584   0 svchost
    185      10     2064       7360       1,70   3612   0 svchost
    135       9     1628       5796       0,02   3696   0 svchost
    243      14     3568      13656       0,92   3708   0 svchost
    432      32    11244      19076      37,38   3996   0 svchost
    696      76    32412      40472      62,30   4196   0 svchost
    356      29    10804      13976      10,17   4292   0 svchost
    539      27    18228      29268      12,16   4336   0 svchost
    428      28    45228      52692     173,88   4368   0 svchost
    273      14     2916       7476      21,94   4480   0 svchost
    501      21     4348      12616      21,67   4504   0 svchost
    128       7     1288       5252       0,03   4524   0 svchost
    215      12     2380       6756       0,25   4540   0 svchost
    238      12     2804      11304       0,66   4584   0 svchost
    432      21     5508      22472       5,30   4644   0 svchost
    203      11     2324       8200       0,88   4828   0 svchost
    136       8     1636       5540       0,14   5128   0 svchost
    397      27     3668      11836       1,02   6012   0 svchost
    138       9     1792       8624       0,11   6220   0 svchost
    174      58     2312       6836       1,75   6488   0 svchost
    159      10     2016       9084       0,11   6512   0 svchost
    183      13     2852      10736       2,78   6952   0 svchost
    272      11     2264      10668       6,86   7448   0 svchost
    150       9     1832       6516       0,11   7684   0 svchost
    315      14     5208      17680      57,66   8440   0 svchost
    211       9     1804       7500       0,25   8520   0 svchost
    257      14     2952      12768       0,13   8992   8 svchost
    469      27     6552      21088       7,28   9028   0 svchost
    228      12     2708       9224       1,19   9548   0 svchost
    291      13     2968      12124       4,09   9616   0 svchost
    500      23     8356      34956       3,94   9688   8 svchost
    167       9     1628       6784       0,02   9788   0 svchost
    231      13     2976      11324      21,20  11196   0 svchost
    329      12     4820      10288       2,92  11552   0 svchost
    106       7     1336       5432       0,16  11564   0 svchost
    141       8     2904       8292       0,06  11568   0 svchost
    270      14     3704      14264       1,91  12136   0 svchost
    254      16     2740       8288       5,75  12276   0 svchost
    211      11     2460       7924       1,64  12444   0 svchost
    194      12     2548      10580       0,03  12596   0 svchost
    220      13     5104      17920       1,22  14776   0 svchost
    293      17     5072      15968      14,05  16176   0 svchost
    479      20    10276      27512      27,17  17200   8 svchost
    340      16     4200      23228       1,80  18300   8 svchost
    266      13     2580      10604       0,22  20276   0 svchost
   5715       0      196       2164   2 953,84      4   0 System
    844      39    22276        208       1,42  11404   8 SystemSettings
    630      33    18436      44616      11,17   2476   0 taskhostw
    302      28     6800      17196       1,02   8448   8 taskhostw
    136       8     1724       7504       0,06  18892   0 unsecapp
    560      24    27892      51260       6,23   1796   8 WindowsInternal.ComposableShell.Experiences.TextInput.InputApp
    178      11     1764       6824       0,23    944   0 wininit
    274      12     2624      10208       0,44  13352   8 winlogon
    991      66    49420         60       2,20   6524   8 WinStore.App
    131       7     1708       6208       4,00   3924   0 wlanext
    545      20    15816      23568      44,20   6208   0 WmiPrvSE
    369      16     9924      19188     144,41   6260   0 WmiPrvSE
    342      15     6796      17364       9,53   1260   0 WUDFHost
   2258      48    43904      51460     485,36   4744   0 XtuService
    575      42    40192      11768       1,16  12716   8 YourPhone
```


**winlogon.exe** permet l'ouverture et la fermeture d'une session windows

**lssas.exe** s'assure de l'identification des utilisateurs, un arrêt du processus peut dans certains cas reboot l'ordinateur

**ssms.exe** (Session Manager Subsystem), ce processus est un processus de démarrage de windows, il permet de démarrer la gestion de la mémoire, le mode noyau du sous-système Win32 et les processus

**svchost** permet au système de charger des bibliothèques de lien dynamique (DLL)

**wininit.exe** permet d'initialiser windows

### Network
```
Get-NetAdapter | fl Name,InterfaceDescription, InterfaceIndex


Name                 : Ethernet
InterfaceDescription : Realtek PCIe GbE Family Controller
InterfaceIndex       : 12

Name                 : Wi-Fi
InterfaceDescription : Intel(R) Wireless-AC 9560 160MHz
InterfaceIndex       : 9

Name                 : Connexion réseau Bluetooth
InterfaceDescription : Bluetooth Device (Personal Area Network)
InterfaceIndex       : 6
```

```
netstat -anob

Connexions actives

  Proto  Adresse locale         Adresse distante       État
  TCP    0.0.0.0:135            0.0.0.0:0              LISTENING       1384
  RpcSs
 [svchost.exe]
  TCP    0.0.0.0:445            0.0.0.0:0              LISTENING       4
 Impossible d’obtenir les informations de propriétaire
  TCP    0.0.0.0:902            0.0.0.0:0              LISTENING       4812
 [vmware-authd.exe]
  TCP    0.0.0.0:912            0.0.0.0:0              LISTENING       4812
 [vmware-authd.exe]
  TCP    0.0.0.0:5040           0.0.0.0:0              LISTENING       9960
  CDPSvc
 [svchost.exe]
  TCP    0.0.0.0:7680           0.0.0.0:0              LISTENING       14356
 Impossible d’obtenir les informations de propriétaire
  TCP    0.0.0.0:9012           0.0.0.0:0              LISTENING       1564
 [ArmourySocketServer.exe]
  TCP    0.0.0.0:9013           0.0.0.0:0              LISTENING       1564
 [ArmourySocketServer.exe]
  TCP    0.0.0.0:27036          0.0.0.0:0              LISTENING       17296
 [steam.exe]
  TCP    0.0.0.0:49664          0.0.0.0:0              LISTENING       8
 [lsass.exe]
  TCP    0.0.0.0:49665          0.0.0.0:0              LISTENING       988
 Impossible d’obtenir les informations de propriétaire
  TCP    0.0.0.0:49666          0.0.0.0:0              LISTENING       1032
  EventLog
 [svchost.exe]
  TCP    0.0.0.0:49667          0.0.0.0:0              LISTENING       2648
  Schedule
 [svchost.exe]
  TCP    0.0.0.0:49668          0.0.0.0:0              LISTENING       3808
 [spoolsv.exe]
  TCP    0.0.0.0:49670          0.0.0.0:0              LISTENING       640
 Impossible d’obtenir les informations de propriétaire
  TCP    10.0.2.2:139           0.0.0.0:0              LISTENING       4
 Impossible d’obtenir les informations de propriétaire
  TCP    10.33.3.41:139         0.0.0.0:0              LISTENING       4
 Impossible d’obtenir les informations de propriétaire
  TCP    10.33.3.41:62563       155.133.248.38:27030   ESTABLISHED     17296
 [steam.exe]
  TCP    10.33.3.41:62606       88.221.41.139:443      CLOSE_WAIT      15408
 [WinStore.App.exe]
  TCP    10.33.3.41:62617       88.221.41.139:443      CLOSE_WAIT      15408
 [WinStore.App.exe]
  TCP    10.33.3.41:62629       40.67.251.132:443      ESTABLISHED     4796
  WpnService
 [svchost.exe]
  TCP    10.33.3.41:62634       40.67.251.132:443      ESTABLISHED     4796
  WpnService
 [svchost.exe]
  TCP    10.33.3.41:62683       151.101.17.44:443      ESTABLISHED     1844
 [msedge.exe]
  TCP    10.33.3.41:62690       40.67.254.36:443       ESTABLISHED     1844
 [msedge.exe]
  TCP    10.33.3.41:62694       18.182.129.64:443      ESTABLISHED     1844
 [msedge.exe]
  TCP    10.33.3.41:62734       172.65.251.78:443      ESTABLISHED     1844
 [msedge.exe]
  TCP    10.33.3.41:62751       152.199.19.161:443     CLOSE_WAIT      13000
 [SearchUI.exe]
  TCP    10.33.3.41:62752       152.199.19.161:443     CLOSE_WAIT      13000
 [SearchUI.exe]
  TCP    10.33.3.41:62753       152.199.19.161:443     CLOSE_WAIT      13000
 [SearchUI.exe]
  TCP    10.33.3.41:62756       51.138.106.75:443      TIME_WAIT       0
  TCP    10.33.3.41:62761       13.107.6.158:443       TIME_WAIT       0
  TCP    10.33.3.41:62762       13.107.21.200:443      TIME_WAIT       0
  TCP    10.33.3.41:62763       185.63.145.5:443       TIME_WAIT       0
  TCP    10.33.3.41:62764       13.107.6.254:443       TIME_WAIT       0
  TCP    10.33.3.41:62765       13.107.42.254:443      TIME_WAIT       0
  TCP    10.33.3.41:62766       13.107.246.10:443      TIME_WAIT       0
  TCP    10.33.3.41:62767       13.107.3.254:443       TIME_WAIT       0
  TCP    10.33.3.41:62768       13.107.18.11:443       TIME_WAIT       0
  TCP    10.33.3.41:62778       51.138.106.75:443      ESTABLISHED     18600
  CDPUserSvc_7fb7d6c
 [svchost.exe]
  TCP    10.33.3.41:62779       3.213.18.157:443       ESTABLISHED     1844
 [msedge.exe]
  TCP    10.33.3.41:62780       3.213.18.157:443       ESTABLISHED     1844
 [msedge.exe]
  TCP    127.0.0.1:1042         0.0.0.0:0              LISTENING       12956
 [asus_framework.exe]
  TCP    127.0.0.1:1043         0.0.0.0:0              LISTENING       12956
 [asus_framework.exe]
  TCP    127.0.0.1:9012         127.0.0.1:62620        ESTABLISHED     1564
 [ArmourySocketServer.exe]
  TCP    127.0.0.1:9487         0.0.0.0:0              LISTENING       4148
 [ArmouryCrate.Service.exe]
  TCP    127.0.0.1:9487         127.0.0.1:62556        ESTABLISHED     4148
 [ArmouryCrate.Service.exe]
  TCP    127.0.0.1:13010        0.0.0.0:0              LISTENING       4148
 [ArmouryCrate.Service.exe]
  TCP    127.0.0.1:13030        0.0.0.0:0              LISTENING       4820
 [ROGLiveService.exe]
  TCP    127.0.0.1:13031        0.0.0.0:0              LISTENING       2336
 [ArmouryCrate.UserSessionHelper.exe]
  TCP    127.0.0.1:17945        0.0.0.0:0              LISTENING       2336
 [ArmouryCrate.UserSessionHelper.exe]
  TCP    127.0.0.1:27060        0.0.0.0:0              LISTENING       17296
 [steam.exe]
  TCP    127.0.0.1:62545        0.0.0.0:0              LISTENING       18036
 [NVIDIA Web Helper.exe]
  TCP    127.0.0.1:62556        127.0.0.1:9487         ESTABLISHED     2336
 [ArmouryCrate.UserSessionHelper.exe]
  TCP    127.0.0.1:62557        0.0.0.0:0              LISTENING       3096
 [SteelSeriesEngine3.exe]
  TCP    127.0.0.1:62558        0.0.0.0:0              LISTENING       3096
 [SteelSeriesEngine3.exe]
  TCP    127.0.0.1:62620        127.0.0.1:9012         ESTABLISHED     2336
 [ArmouryCrate.UserSessionHelper.exe]
  TCP    127.0.0.1:62622        127.0.0.1:65001        ESTABLISHED     4784
 [nvcontainer.exe]
  TCP    127.0.0.1:65001        0.0.0.0:0              LISTENING       4784
 [nvcontainer.exe]
  TCP    127.0.0.1:65001        127.0.0.1:62622        ESTABLISHED     4784
 [nvcontainer.exe]
  TCP    192.168.127.1:139      0.0.0.0:0              LISTENING       4
 Impossible d’obtenir les informations de propriétaire
  TCP    192.168.236.1:139      0.0.0.0:0              LISTENING       4
 Impossible d’obtenir les informations de propriétaire
  TCP    [::]:135               [::]:0                 LISTENING       1384
  RpcSs
 [svchost.exe]
  TCP    [::]:445               [::]:0                 LISTENING       4
 Impossible d’obtenir les informations de propriétaire
  TCP    [::]:7680              [::]:0                 LISTENING       14356
 Impossible d’obtenir les informations de propriétaire
  TCP    [::]:9012              [::]:0                 LISTENING       1564
 [ArmourySocketServer.exe]
  TCP    [::]:9013              [::]:0                 LISTENING       1564
 [ArmourySocketServer.exe]
  TCP    [::]:49664             [::]:0                 LISTENING       8
 [lsass.exe]
  TCP    [::]:49665             [::]:0                 LISTENING       988
 Impossible d’obtenir les informations de propriétaire
  TCP    [::]:49666             [::]:0                 LISTENING       1032
  EventLog
 [svchost.exe]
  TCP    [::]:49667             [::]:0                 LISTENING       2648
  Schedule
 [svchost.exe]
  TCP    [::]:49668             [::]:0                 LISTENING       3808
 [spoolsv.exe]
  TCP    [::]:49670             [::]:0                 LISTENING       640
 Impossible d’obtenir les informations de propriétaire
  TCP    [::1]:49669            [::]:0                 LISTENING       5180
 [jhi_service.exe]
  UDP    0.0.0.0:500            *:*                                    4396
  IKEEXT
 [svchost.exe]
  UDP    0.0.0.0:4500           *:*                                    4396
  IKEEXT
 [svchost.exe]
  UDP    0.0.0.0:5050           *:*                                    9960
  CDPSvc
 [svchost.exe]
  UDP    0.0.0.0:5353           *:*                                    9780
 [msedge.exe]
  UDP    0.0.0.0:5353           *:*                                    9780
 [msedge.exe]
  UDP    0.0.0.0:5353           *:*                                    9780
 [msedge.exe]
  UDP    0.0.0.0:5353           *:*                                    9780
 [msedge.exe]
  UDP    0.0.0.0:5353           *:*                                    9780
 [msedge.exe]
  UDP    0.0.0.0:5353           *:*                                    9780
 [msedge.exe]
  UDP    0.0.0.0:5353           *:*                                    9780
 [msedge.exe]
  UDP    0.0.0.0:5353           *:*                                    9780
 [msedge.exe]
  UDP    0.0.0.0:5353           *:*                                    2440
  Dnscache
 [svchost.exe]
  UDP    0.0.0.0:5355           *:*                                    2440
  Dnscache
 [svchost.exe]
  UDP    0.0.0.0:27036          *:*                                    17296
 [steam.exe]
  UDP    0.0.0.0:53466          *:*                                    4784
 [nvcontainer.exe]
  UDP    10.0.2.2:137           *:*                                    4
 Impossible d’obtenir les informations de propriétaire
  UDP    10.0.2.2:138           *:*                                    4
 Impossible d’obtenir les informations de propriétaire
  UDP    10.0.2.2:1900          *:*                                    8500
  SSDPSRV
 [svchost.exe]
  UDP    10.0.2.2:5353          *:*                                    4784
 [nvcontainer.exe]
  UDP    10.0.2.2:50730         *:*                                    8500
  SSDPSRV
 [svchost.exe]
  UDP    10.33.3.41:137         *:*                                    4
 Impossible d’obtenir les informations de propriétaire
  UDP    10.33.3.41:138         *:*                                    4
 Impossible d’obtenir les informations de propriétaire
  UDP    10.33.3.41:1900        *:*                                    8500
  SSDPSRV
 [svchost.exe]
  UDP    10.33.3.41:5353        *:*                                    4784
 [nvcontainer.exe]
  UDP    10.33.3.41:50733       *:*                                    8500
  SSDPSRV
 [svchost.exe]
  UDP    127.0.0.1:1900         *:*                                    8500
  SSDPSRV
 [svchost.exe]
  UDP    127.0.0.1:10090        *:*                                    18036
 [NVIDIA Web Helper.exe]
  UDP    127.0.0.1:50734        *:*                                    8500
  SSDPSRV
 [svchost.exe]
  UDP    127.0.0.1:54920        *:*                                    18132
 [nvcontainer.exe]
  UDP    127.0.0.1:58623        *:*                                    2432
  NlaSvc
 [svchost.exe]
  UDP    127.0.0.1:64023        *:*                                    4556
  iphlpsvc
 [svchost.exe]
  UDP    192.168.127.1:137      *:*                                    4
 Impossible d’obtenir les informations de propriétaire
  UDP    192.168.127.1:138      *:*                                    4
 Impossible d’obtenir les informations de propriétaire
  UDP    192.168.127.1:1900     *:*                                    8500
  SSDPSRV
 [svchost.exe]
  UDP    192.168.127.1:5353     *:*                                    4784
 [nvcontainer.exe]
  UDP    192.168.127.1:50731    *:*                                    8500
  SSDPSRV
 [svchost.exe]
  UDP    192.168.236.1:137      *:*                                    4
 Impossible d’obtenir les informations de propriétaire
  UDP    192.168.236.1:138      *:*                                    4
 Impossible d’obtenir les informations de propriétaire
  UDP    192.168.236.1:1900     *:*                                    8500
  SSDPSRV
 [svchost.exe]
  UDP    192.168.236.1:5353     *:*                                    4784
 [nvcontainer.exe]
  UDP    192.168.236.1:50732    *:*                                    8500
  SSDPSRV
 [svchost.exe]
  UDP    [::]:500               *:*                                    4396
  IKEEXT
 [svchost.exe]
  UDP    [::]:4500              *:*                                    4396
  IKEEXT
 [svchost.exe]
  UDP    [::]:5353              *:*                                    9780
 [msedge.exe]
  UDP    [::]:5353              *:*                                    9780
 [msedge.exe]
  UDP    [::]:5353              *:*                                    9780
 [msedge.exe]
  UDP    [::]:5353              *:*                                    9780
 [msedge.exe]
  UDP    [::]:5353              *:*                                    2440
  Dnscache
 [svchost.exe]
  UDP    [::]:5355              *:*                                    2440
  Dnscache
 [svchost.exe]
  UDP    [::]:53467             *:*                                    4784
 [nvcontainer.exe]
  UDP    [::1]:1900             *:*                                    8500
  SSDPSRV
 [svchost.exe]
  UDP    [::1]:5353             *:*                                    4784
 [nvcontainer.exe]
  UDP    [::1]:50729            *:*                                    8500
  SSDPSRV
 [svchost.exe]
  UDP    [fe80::3491:8ddc:ff4d:a4ac%15]:1900  *:*                                    8500
  SSDPSRV
 [svchost.exe]
  UDP    [fe80::3491:8ddc:ff4d:a4ac%15]:50727  *:*                                    8500
  SSDPSRV
 [svchost.exe]
  UDP    [fe80::6932:170a:4db1:b99d%11]:1900  *:*                                    8500
  SSDPSRV
 [svchost.exe]
  UDP    [fe80::6932:170a:4db1:b99d%11]:50725  *:*                                    8500
  SSDPSRV
 [svchost.exe]
  UDP    [fe80::cc9f:e6bf:8e24:cfe2%12]:1900  *:*                                    8500
  SSDPSRV
 [svchost.exe]
  UDP    [fe80::cc9f:e6bf:8e24:cfe2%12]:50728  *:*                                    8500
  SSDPSRV
 [svchost.exe]
  UDP    [fe80::e000:bd63:1b2d:888e%5]:1900  *:*                                    8500
  SSDPSRV
 [svchost.exe]
  UDP    [fe80::e000:bd63:1b2d:888e%5]:50726  *:*                                    8500
  SSDPSRV
 [svchost.exe]
 ```

## II/ Scripting
### Script 1
```
#Kraizix
#19/10/2020
#Resolution of TP1 https://gitlab.com/it4lik/b1-workstation/-/tree/master/tp/1
#This script shows various information about your computer

"Get average ping latency to 10.33.3.253 server..."
""
$rep_time = (Test-Connection -ComputerName "10.33.3.253" -Count 4  | measure-Object -Property ResponseTime -Average).average

"Get download and upload speed..."
""
$DownloadURL = "https://bintray.com/ookla/download/download_file?file_path=ookla-speedtest-1.0.0-win64.zip"
$DownloadLocation = "$($Env:ProgramData)\SpeedtestCLI"
try {
    $TestDownloadLocation = Test-Path $DownloadLocation
    if (!$TestDownloadLocation) {
        new-item $DownloadLocation -ItemType Directory -force
        Invoke-WebRequest -Uri $DownloadURL -OutFile "$($DownloadLocation)\speedtest.zip"
        Expand-Archive "$($DownloadLocation)\speedtest.zip" -DestinationPath $DownloadLocation -Force
    } 
}
catch {  
    write-host "The download and extraction of SpeedtestCLI failed. Error: $($_.Exception.Message)"
    exit 1
}
$SpeedtestResults = & "$($DownloadLocation)\speedtest.exe" --format=json --accept-license --accept-gdpr
$SpeedtestResults | Out-File "$($DownloadLocation)\LastResults.txt" -Force
$SpeedtestResults = $SpeedtestResults | ConvertFrom-Json
 
[PSCustomObject]$SpeedtestObj = @{
    downloadspeed = [math]::Round($SpeedtestResults.download.bandwidth / 1000000 * 8, 2)
    uploadspeed   = [math]::Round($SpeedtestResults.upload.bandwidth / 1000000 * 8, 2)
}
Write-Host -NoNewline "Name : "
[system.environment]::MachineName
""
Write-Host -NoNewLine "Ip : "
Write-Host $(ipconfig | Where-Object {$_ -match 'IPv4.+\s(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})' } | out-null; $Matches[1])
""
Write-Host -NoNewline "OS : "
(Get-WmiObject Win32_OperatingSystem).Caption
""
Write-Host -NoNewline "Version : "
(Get-WmiObject Win32_OperatingSystem).Version
""
Get-Command -Module WindowsUpdateProvider
""
$Updates = Start-WUScan -SearchCriteria "IsInstalled=0 AND IsHidden=0 AND IsAssigned=1"
if (!$Updates)
{
    "Is OS up-to-date : True"
}
else
{
    "Is OS up-to-date : False"
}
$Uptime = $(Get-WmiObject Win32_OperatingSystem).LastBootUpTime
""
Write-Host "Uptime : $Uptime"
""
$Taille_RAM_MAX=[STRING]((Get-WmiObject -Class Win32_ComputerSystem ).TotalPhysicalMemory/1GB)
$Taille_RAM_LIBRE=[STRING]((Get-WmiObject -Class Win32_OperatingSystem).FreePhysicalMemory/1MB)
$Taille_RAM_UTILISE=[STRING]($Taille_RAM_MAX - $Taille_RAM_LIBRE)
"RAM :"
Write-Host "    Free : $Taille_RAM_LIBRE Gb"
Write-Host "    Used : $Taille_RAM_UTILISE Gb"
$Taille_Disk_Max = [STRING]((Get-WmiObject Win32_LogicalDisk).Size/1GB)
$Taille_Disk_Free = [STRING]((Get-WmiObject Win32_LogicalDisk).FreeSpace/1GB)
$Taille_Disk_Utilise=[STRING]($Taille_Disk_Max - $Taille_Disk_Free)
""
"Disk :"
Write-Host "    Free : $Taille_Disk_Free Gb"
Write-Host "    Used : $Taille_Disk_Utilise Gb"
""
$Users =(Get-LocalUser).Name
Write-Host "Users : $Users"
""
Write-Host "10.33.3.253 Average Ping : $rep_time ms"
$download =$SpeedtestObj.downloadspeed
$upload = $SpeedtestObj.uploadspeed
Write-Host "Download speed : $download Mbps"
Write-Host "Upload speed : $upload Mbps"
```

```
PS C:\Users\kevin\AppData\Local\Packages\CanonicalGroupLimited.Ubuntu20.04onWindows_79rhkp1fndgsc\LocalState\rootfs\home\kraizix\B1-infrasi\b1_workstation> .\script1_Granger_Kévin.ps1

Get average ping latency to 10.33.3.253 server...

Get download and upload speed...

Name : LAPTOP-DS2IE5NI

Ip : 192.168.1.55

OS : Microsoft Windows 10 Famille

Version : 10.0.18363


CommandType     Name                                               Version    Source
-----------     ----                                               -------    ------
Function        Get-WUAVersion                                     1.0.0.2    WindowsUpdateProvider
Function        Get-WUIsPendingReboot                              1.0.0.2    WindowsUpdateProvider
Function        Get-WULastInstallationDate                         1.0.0.2    WindowsUpdateProvider
Function        Get-WULastScanSuccessDate                          1.0.0.2    WindowsUpdateProvider
Function        Install-WUUpdates                                  1.0.0.2    WindowsUpdateProvider
Function        Start-WUScan                                       1.0.0.2    WindowsUpdateProvider

Is OS up-to-date : False

Uptime : 20201113133235.500000+060

RAM :
    Free : 8.15879440307617 Gb
    Used : 7.69715499877933 Gb

Disk :
    Free : 310.416900634766 Gb
    Used : 164.933017730713 Gb

Users : Administrateur DefaultAccount Invité kevin WDAGUtilityAccount

10.33.3.253 Average Ping : 21 ms
Download speed : 288.43 Mbps
Upload speed : 176.16 Mbps  
```
### Script 2
```
#Kraizix
#19/10/2020
#Resolution of TP1 https://gitlab.com/it4lik/b1-workstation/-/tree/master/tp/1
#This script allows you to shutdown/lock your pc

$function=[STRING]$args[0]
$time=[INT]$args[1]
$sleep = "lock"
$shutdown = "shutdown"
Write-Host $function
if ($function -eq $sleep)
{
    Start-Sleep -s $time
    $xCmdString = {rundll32.exe user32.dll,LockWorkStation}
    Invoke-Command $xCmdString
}
if ($function -eq $shutdown)
{
    Start-Sleep -s $time
    Stop-Computer -ComputerName localhost
}
```

## III/ Gestion de softs
Un gestionnaire de paquet permet d'automatiser l'installation et la désinstallation d'un programme ainsi que les mises à jour de logiciels installés sur le pc, il va aussi installer tous les programmes tiers que le programme nécessite. Le gestionnaire de paquet permet aussi d'éviter les PUP,adwares et malwares que l'on pourrais télécharger depuis un site malveillant, car le logiciel provient directement du dépôt du créateur

On utilise ```choco list ```afin de lister tout les paquets
```
PS C:\Windows\system32> choco list
Chocolatey v0.10.15
[...]
5676 packages found.
```
Pour la source des paquets on utilise ```choco source```
```
PS C:\Windows\system32> choco source
Chocolatey v0.10.15
chocolatey - https://chocolatey.org/api/v2/ | Priority 0|Bypass Proxy - False|Self-Service - False|Admin Only - False.
```
## IV/ Partage de fichiers
### 3 : 
```
PS C:\Windows\system32> ssh root@192.168.120.53
root@192.168.120.53's password:
Last login: Sun Nov 15 15:10:53 2020
[root@localhost ~]#
```
### 4 :
![](https://i.imgur.com/RI97Kpn.png)

![](https://i.imgur.com/NYoyQqq.png)


