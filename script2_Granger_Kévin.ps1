#Kraizix
#19/10/2020
#Resolution of TP1 https://gitlab.com/it4lik/b1-workstation/-/tree/master/tp/1
#This script allows you to shutdown/lock your pc

$function=[STRING]$args[0]
$time=[INT]$args[1]
$sleep = "lock"
$shutdown = "shutdown"
Write-Host $function
if ($function -eq $sleep)
{
    Start-Sleep -s $time
    $xCmdString = {rundll32.exe user32.dll,LockWorkStation}
    Invoke-Command $xCmdString
}
if ($function -eq $shutdown)
{
    Start-Sleep -s $time
    Stop-Computer -ComputerName localhost
}